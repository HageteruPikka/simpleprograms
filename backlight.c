#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
  FILE *sysfs_backlight;
  int  brightness;
  int  diff;

  /* printf("argv[1]=%s\n",argv[1]); */

  diff= atoi( argv[1] );
  /* printf("diff=%d\n",diff); */

  sysfs_backlight= fopen("/sys/class/backlight/amdgpu_bl0/brightness","r");

    fscanf(sysfs_backlight, "%d", &brightness);

  fclose(sysfs_backlight);

  brightness = brightness + diff;

  if ( brightness > 255 ) {
    brightness = 255;
  } else if ( brightness < 0 ) {
    brightness = 0;
  }

  /* printf("%d\n", brightness); */

  sysfs_backlight= fopen("/sys/class/backlight/amdgpu_bl0/brightness","w");

    fprintf( sysfs_backlight, "%d", brightness );

  fclose(sysfs_backlight);


  return 0;
}


/* vim: set fdm=marker: */
