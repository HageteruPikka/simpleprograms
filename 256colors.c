#include <stdio.h>

int main(void)
{
  int i;
  for (i=0; i<=255; i++) {
    printf("\e[38;5;%dm %03d",i,i);
    if ( i%16 == 15) printf("\n");
  }
  return 0;
}


/* vim: set fdm=marker: */
