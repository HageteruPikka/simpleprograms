#include <stdio.h>

int main(void)
{
  FILE *loadavg_file;
  float load1, load2, load3;
  char  str[16];
  int num;

  loadavg_file = fopen("/proc/loadavg","r");
    fscanf(loadavg_file, "%f %f %f %s %d", &load1, &load2, &load3, str, &num);
  fclose(loadavg_file);

  load1=load1 * 100.;

  printf("%3.0f\%\n",load1);

  return 0;
}

/* vim: set fdm=marker: */
