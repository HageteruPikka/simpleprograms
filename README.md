# Simple Programs

I write the programs in the repo for my learning. There are a tons of other better, well
maintained products.

* batterycheck.c: just check capacity and plug-in or plug-off status for shell prompts.
* networkcheck.c: just check LAN and Wifi status.
* backlight.c:    Brightness control.
* 256colors.c:    Show 256colors on terminal.
