#include <stdio.h>
#include <unistd.h>

int main(void)
{
  FILE *thermal_file;
  FILE *gov_cpu0_file;
  FILE *gov_cpu1_file;
  FILE *gov_cpu2_file;
  FILE *gov_cpu3_file;
  FILE *fan_pwm1;
  FILE *fan_pwm1_enable;
  int  temperature;
  int  sleep_time = 5; /* sec */

  /* 
   design
   if T>90 then use powersave and keep fan full throttling.
   if T>70 then full throttle the fan, but use ondemand.
   if T>60 then 3/4 throttle the fan, but use ondemand.
   if T>50 then 1/2 throttle the fan, but use ondemand.
   if T=<50, then use ondemand and automatic fan control.
  */
	/* /sys/devices/pci0000:00/0000:00:18.3/hwmon/hwmon1/temp1_max 
     = 70
			
		 /sys/devices/pci0000:00/0000:00:18.3/hwmon/hwmon1/temp1_crit_hyst
     =104
  */

  while(1)
  {
    thermal_file = fopen("/sys/class/thermal/thermal_zone0/temp","r");
    fscanf(thermal_file, "%d", &temperature);
    fclose(thermal_file);

    if ( temperature > 90000 ) { 

      gov_cpu0_file = fopen("/sys/devices/system/cpu/cpufreq/policy0/scaling_governor","w");
      fprintf( gov_cpu0_file, "powersave");
      fclose(gov_cpu0_file);

      gov_cpu1_file = fopen("/sys/devices/system/cpu/cpufreq/policy1/scaling_governor","w");
      fprintf( gov_cpu1_file, "powersave");
      fclose(gov_cpu1_file);

      gov_cpu2_file = fopen("/sys/devices/system/cpu/cpufreq/policy2/scaling_governor","w");
      fprintf( gov_cpu2_file, "powersave");
      fclose(gov_cpu2_file);
      
      gov_cpu3_file = fopen("/sys/devices/system/cpu/cpufreq/policy3/scaling_governor","w");
      fprintf( gov_cpu3_file, "powersave");
      fclose(gov_cpu3_file);

      fan_pwm1_enable = fopen("/sys/devices/platform/asus-nb-wmi/hwmon/hwmon2/pwm1_enable","w");
      fprintf( fan_pwm1_enable, "1");
      fclose(fan_pwm1_enable);

      fan_pwm1 = fopen("/sys/devices/platform/asus-nb-wmi/hwmon/hwmon2/pwm1","w");
      fprintf( fan_pwm1, "255");
      fclose(fan_pwm1);

    } else if (temperature > 70000 ) {

      gov_cpu0_file = fopen("/sys/devices/system/cpu/cpufreq/policy0/scaling_governor","w");
      fprintf( gov_cpu0_file, "ondemand");
      fclose(gov_cpu0_file);

      gov_cpu1_file = fopen("/sys/devices/system/cpu/cpufreq/policy1/scaling_governor","w");
      fprintf( gov_cpu1_file, "ondemand");
      fclose(gov_cpu1_file);

      gov_cpu2_file = fopen("/sys/devices/system/cpu/cpufreq/policy2/scaling_governor","w");
      fprintf( gov_cpu2_file, "ondemand");
      fclose(gov_cpu2_file);
      
      gov_cpu3_file = fopen("/sys/devices/system/cpu/cpufreq/policy3/scaling_governor","w");
      fprintf( gov_cpu3_file, "ondemand");
      fclose(gov_cpu3_file);

      fan_pwm1_enable = fopen("/sys/devices/platform/asus-nb-wmi/hwmon/hwmon2/pwm1_enable","w");
      fprintf( fan_pwm1_enable, "1");
      fclose(fan_pwm1_enable);

      fan_pwm1 = fopen("/sys/devices/platform/asus-nb-wmi/hwmon/hwmon2/pwm1","w");
      fprintf( fan_pwm1, "255");
      fclose(fan_pwm1);

    } else if (temperature > 60000 ) {

      gov_cpu0_file = fopen("/sys/devices/system/cpu/cpufreq/policy0/scaling_governor","w");
      fprintf( gov_cpu0_file, "ondemand");
      fclose(gov_cpu0_file);

      gov_cpu1_file = fopen("/sys/devices/system/cpu/cpufreq/policy1/scaling_governor","w");
      fprintf( gov_cpu1_file, "ondemand");
      fclose(gov_cpu1_file);

      gov_cpu2_file = fopen("/sys/devices/system/cpu/cpufreq/policy2/scaling_governor","w");
      fprintf( gov_cpu2_file, "ondemand");
      fclose(gov_cpu2_file);
      
      gov_cpu3_file = fopen("/sys/devices/system/cpu/cpufreq/policy3/scaling_governor","w");
      fprintf( gov_cpu3_file, "ondemand");
      fclose(gov_cpu3_file);

      fan_pwm1_enable = fopen("/sys/devices/platform/asus-nb-wmi/hwmon/hwmon2/pwm1_enable","w");
      fprintf( fan_pwm1_enable, "1");
      fclose(fan_pwm1_enable);

      fan_pwm1 = fopen("/sys/devices/platform/asus-nb-wmi/hwmon/hwmon2/pwm1","w");
      fprintf( fan_pwm1, "192");
      fclose(fan_pwm1);

    } else if (temperature > 50000 ) {

      gov_cpu0_file = fopen("/sys/devices/system/cpu/cpufreq/policy0/scaling_governor","w");
      fprintf( gov_cpu0_file, "ondemand");
      fclose(gov_cpu0_file);

      gov_cpu1_file = fopen("/sys/devices/system/cpu/cpufreq/policy1/scaling_governor","w");
      fprintf( gov_cpu1_file, "ondemand");
      fclose(gov_cpu1_file);

      gov_cpu2_file = fopen("/sys/devices/system/cpu/cpufreq/policy2/scaling_governor","w");
      fprintf( gov_cpu2_file, "ondemand");
      fclose(gov_cpu2_file);
      
      gov_cpu3_file = fopen("/sys/devices/system/cpu/cpufreq/policy3/scaling_governor","w");
      fprintf( gov_cpu3_file, "ondemand");
      fclose(gov_cpu3_file);

      fan_pwm1_enable = fopen("/sys/devices/platform/asus-nb-wmi/hwmon/hwmon2/pwm1_enable","w");
      fprintf( fan_pwm1_enable, "1");
      fclose(fan_pwm1_enable);

      fan_pwm1 = fopen("/sys/devices/platform/asus-nb-wmi/hwmon/hwmon2/pwm1","w");
      fprintf( fan_pwm1, "128");
      fclose(fan_pwm1);

    } else {

      gov_cpu0_file = fopen("/sys/devices/system/cpu/cpufreq/policy0/scaling_governor","w");
      fprintf( gov_cpu0_file, "ondemand");
      fclose(gov_cpu0_file);

      gov_cpu1_file = fopen("/sys/devices/system/cpu/cpufreq/policy1/scaling_governor","w");
      fprintf( gov_cpu1_file, "ondemand");
      fclose(gov_cpu1_file);

      gov_cpu2_file = fopen("/sys/devices/system/cpu/cpufreq/policy2/scaling_governor","w");
      fprintf( gov_cpu2_file, "ondemand");
      fclose(gov_cpu2_file);
      
      gov_cpu3_file = fopen("/sys/devices/system/cpu/cpufreq/policy3/scaling_governor","w");
      fprintf( gov_cpu3_file, "ondemand");
      fclose(gov_cpu3_file);

      fan_pwm1_enable = fopen("/sys/devices/platform/asus-nb-wmi/hwmon/hwmon2/pwm1_enable","w");
      fprintf( fan_pwm1_enable, "2");
      fclose(fan_pwm1_enable);

    }	
    sleep( sleep_time );
  }

  return 0;
}
/* vim: set fdm=marker: */
