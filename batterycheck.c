#include <stdio.h>

int main(void)
{
  FILE *capacity_file;
  FILE *status_file;
  int  capacity;
  int  status;

  capacity_file = fopen("/sys/class/power_supply/BAT1/capacity","r");
    fscanf(capacity_file, "%d", &capacity);
  fclose(capacity_file);

  status_file = fopen("/sys/class/power_supply/ACAD/online","r");
    fscanf(status_file, "%d", &status);
  fclose(status_file);

  if ( status == 1 ) {
    printf("%d%+\n",capacity);
  } else {
    printf("%d%-\n",capacity);
  }

  return 0;
}


/* vim: set fdm=marker: */
