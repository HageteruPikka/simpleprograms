/*
 *  mbdf -- merge BDF file, make multilingual BDF font
 *
 *  revision history
 *	0.0: May   6, 2003 by Dai ISHIJIMA
 *	1.0: Nov.  9, 2015
 */

/*
 * Copied from http://www.wheel.gr.jp/~dai/fonts/bdf_fbsd_vt.html
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define YES 1
#define NO  0


#define MAXLINE 1024

#define MAXFONT 8
#define MAXCHAR 65535
#define UNDEF -1

char *prog;
#define shift --argc; ++argv

int verbose = NO;

#define match(s1,s2) strncmp(s1, s2, strlen(s2))


/* ファイルテーブルの初期化 */
void initencoding(int *encoding, int *ffilenum, int *ffilepos)
{
    int i;

    for (i = 0; i < MAXCHAR; i++) {
	encoding[i] = UNDEF;
	ffilenum[i] = UNDEF;
	ffilepos[i] = UNDEF;
    }
}


/* エンコーディング表の初期化 */
void initcc(int *native2unicode, int *unicode2native)
{
    int i;

    for (i = 0; i < MAXCHAR; i++) {
	native2unicode[i] = UNDEF;
	unicode2native[i] = UNDEF;
    }
}


/* エンコーディング表の初期化 */
void initmapping(int *native2unicode, int *unicode2native)
{
    int i;

    for (i = 0; i < MAXCHAR; i++) {
	native2unicode[i] = i;
	unicode2native[i] = i;
    }
}


/* エンコーディング表を読む */
void readencoding(FILE *fp, int *native2unicode, int *unicode2native)
{
    char s[MAXLINE];
    unsigned short unicode, native;
    
    while (fgets(s, MAXLINE, fp) != NULL) {
	if (sscanf(s, "%hx %hx", &unicode, &native) == 2) {
	    if (unicode2native[unicode] == UNDEF) {
		native2unicode[native] = unicode;
		unicode2native[unicode] = native;
	    }
	    else if (verbose) {
		fprintf(stderr, "dup: %04x -> %04x, %04x\n",
			unicode, unicode2native[unicode], native);
	    }
	}
    }
}


/* フォントファイルを読んで、各文字の位置を覚えておく */
void parsefont(FILE *fp, int *native2unicode, int *unicode2native,
	       int *encoding, int *ffilenum, int *ffilepos,
	       int filenum)
{
    char s[MAXLINE];
    int n, m;
    unsigned short code;
    int unicode;
    int pos;
    int converted;

    pos = ftell(fp);
    n = m = 0;
    while (fgets(s, MAXLINE, fp) != NULL) {
	if (sscanf(s, "ENCODING %hd", &code) == 1) {
	    ++n;
	    converted = 0;
	    for (unicode = 0; unicode < MAXCHAR; unicode++) {
		if ((encoding[unicode] == UNDEF)
		    && (unicode2native[unicode] == code)) {
		    encoding[unicode] = code;
		    ffilepos[unicode] = pos;
		    ffilenum[unicode] = filenum;
		    ++m;
		    ++converted;
		}
	    }
	    if ((verbose) && (converted != 1)) {
		fprintf(stderr, "%04x converted %d\n", code, converted);
	    }
	}
	pos = ftell(fp);
    }
    if (verbose) {
	fprintf(stderr,
		"%d chars read, %d chars convert, %d chars unconverted\n",
		n, m, n - m);
    }
    rewind(fp);
}


/* 文字数を数える */
int countchars(int *encoding)
{
    int i;
    int n;

    n = 0;
    for (i = 0; i < MAXCHAR; i++) {
	if (encoding[i] != UNDEF) {
	    ++n;
	}
    }
    return(n);
}


void usage()
{
    fprintf(stderr, "Usage: %s [<-c encoding font> | <-f font>]...\n", prog);
}


int main(int argc, char *argv[])
{
    int n;
    int native2unicode[MAXCHAR];
    int unicode2native[MAXCHAR];
    int encoding[MAXCHAR];
    int ffilenum[MAXCHAR];
    int ffilepos[MAXCHAR];
    FILE *fontfile[MAXFONT];
    FILE *fp;
    int nchars;
    char s[MAXLINE];
    int i;

    prog = *argv;
    shift;
    initencoding(encoding, ffilenum, ffilepos);
    n = 0;
    while ((n < MAXFONT) && (argc > 0) && (argv[0][0] == '-')) {
	if (argv[0][1] == 'c') {
	    shift;
	    initcc(native2unicode, unicode2native);
	    if ((fp = fopen(*argv, "r")) != NULL) {
		readencoding(fp, native2unicode, unicode2native);
		shift;
		if ((fontfile[n] = fopen(*argv, "r")) != NULL) {
		    parsefont(fontfile[n], native2unicode, unicode2native,
			      encoding, ffilenum, ffilepos, n);
		    shift;
		    ++n;
		}
		else {
		    fprintf(stderr, "%s: can't open %s\n", prog, *argv);
		    exit(1);
		}
		fclose(fp);
	    }
	    else {
		fprintf(stderr, "%s: can't open %s\n", prog, *argv);
		exit(1);
	    }
	}
	else if (argv[0][1] == 'f') {
	    shift;
	    initmapping(native2unicode, unicode2native);
	    if ((fontfile[n] = fopen(*argv, "r")) != NULL) {
		parsefont(fontfile[n], native2unicode, unicode2native,
			  encoding, ffilenum, ffilepos, n);
		shift;
		++n;
	    }
	    else {
		fprintf(stderr, "%s: can't open %s\n", prog, *argv);
		exit(1);
	    }
	}
	else if (argv[0][1] == 'v') {
	    ++verbose;
	    shift;
	}
	else {
	    usage();
	    exit(1);
	}
    }
    rewind(fontfile[0]);
    while (fgets(s, MAXLINE, fontfile[0]) != NULL) {
	if (sscanf(s, "CHARS %d", &nchars) == 1) {
	    break;
	}
	fputs(s, stdout);
    }
    nchars = countchars(encoding);
    printf("CHARS %d\n", nchars);
    for (i = 0; i < MAXCHAR; i++) {
	if (encoding[i] != UNDEF) {
	    fseek(fontfile[ffilenum[i]], ffilepos[i], SEEK_SET);
	    while (fgets(s, MAXLINE, fontfile[ffilenum[i]]) != NULL) {
		if  (match(s, "ENCODING") == 0) {
		    printf("STARTCHAR %04x\n", i);
		    printf("ENCODING %d\n", i);
		}
		else {
		    fputs(s, stdout);
		}
		if (match(s, "ENDCHAR") == 0) {
		    break;
		}
	    }
	}
    }
    printf("ENDFONT\n");
    exit(0);
}

/* Local Variables: */
/* compile-command:"cc -Wall -o mbdf2 mbdf2.c" */
/* End: */
